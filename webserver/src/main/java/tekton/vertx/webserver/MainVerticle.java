package tekton.vertx.webserver;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;

import static labs.tekton.vertx.utilities.StringUtils.join;
import static labs.tekton.vertx.utilities.StringUtils.split;

import labs.tekton.vertx.list.LinkedList;

import org.apache.commons.text.WordUtils;

public class MainVerticle extends AbstractVerticle {

    private static final int DEFAULT_PORT = 5000;

    static final String MESSAGE = "Hello from Vert.x!";

    public static String getMessage() {
        return WordUtils.capitalize(join(split(MESSAGE)));
    }

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        vertx.createHttpServer().requestHandler(req -> {
            req.response().putHeader("content-type", "text/plain").end(getMessage());
        }).listen(DEFAULT_PORT, http -> {
            if (http.succeeded()) {
                startPromise.complete();
                System.out.println("HTTP server started on port 8888");
            } else {
                startPromise.fail(http.cause());
            }
        });
    }
}
