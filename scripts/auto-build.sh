#!/usr/bin/env bash

set -eu
set -o pipefail

REPO_PREFIX=registry.gitlab.com/watheia/labs/tekton-vertx

readonly ROOT_DIR="$(cd "$(dirname "${0}")/.." && pwd)"
readonly BUILD_DIR="${ROOT_DIR}/build"
readonly BIN_DIR="${BUILD_DIR}/bin"

# shellcheck source=SCRIPTDIR/.util/tools.sh
source "${ROOT_DIR}/scripts/.util/tools.sh"

# shellcheck source=SCRIPTDIR/.util/print.sh
source "${ROOT_DIR}/scripts/.util/print.sh"

function usage() {
  cat <<-USAGE
package.sh --version <version> [OPTIONS]

Packages the buildpack into a buildpackage .cnb file.

OPTIONS
  --help               -h            prints the command usage
USAGE
}

function repo::prepare() {
  util::print::title "Preparing build..."

  rm -rf "${BUILD_DIR}"

  mkdir -p "${BIN_DIR}"
  mkdir -p "${BUILD_DIR}"

  export PATH="${BIN_DIR}:${PATH}"
}

function pack::build() {
  local version="${1}"
  local prefix="${2}"

  if [[ -z "${version:-}" ]]; then
    version="latest"
  fi

  util::print::title "Starting build..."
  pack build "${prefix}/webserver" \
    --env-file "${ROOT_DIR}/.env" \
    --builder "paketobuildpacks/builder:full" \
    --path "${ROOT_DIR}" \
    --verbose
  # --publish
}

function main() {
  local version=latest
  local prefix="${REPO_PREFIX}"

  while [[ "${#}" != 0 ]]; do
    case "${1}" in

    --version | -v)
      version="${2}"
      shift 2
      ;;

    --prefix | -p)
      prefix="${2}"
      shift 2
      ;;

    --help | -h)
      shift 1
      usage
      exit 0
      ;;

    "")
      # skip if the argument is empty
      shift 1
      ;;

    *)
      util::print::error "unknown argument \"${1}\""
      ;;
    esac
  done

  repo::prepare

  if [[ ! -f "${BIN_DIR}/pack" ]]; then
    util::tools::pack::install --directory "${BIN_DIR}"
  fi

  pack::build "${version}" "${prefix}"
}

main "${@:-}"
